import React from 'react'
import { Link } from 'react-router-dom'
class App extends React.Component {
  render() {
    return (
      <div>
        <h1>Home Page</h1>
        <Link to="/about">About Page</Link>
      </div>
    )
  }
}
export default App;
